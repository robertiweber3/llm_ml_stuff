#!/bin/bash

curl -s -L https://nvidia.github.io/libnvidia-container/rhel8.7/libnvidia-container.repo | sudo tee /etc/yum.repos.d/libnvidia-container.repo

dnf install nvidia-container-toolkit container-selinux.noarch -y

setsebool -P container_use_devices on

cat <<EOF > nvidia-container-microshift.te
module nvidia-container-microshift 1.0;

require {
              type xserver_misc_device_t;
              type container_t;
              class chr_file { map read write };
}

============= container_t ==============
allow container_t xserver_misc_device_t:chr_file map;
EOF

checkmodule -m -M -o nvidia-container-microshift.mod nvidia-container-microshift.te

semodule_package --outfile nvidia-container-microshift.pp --module nvidia-container-microshift.mod

semodule -i nvidia-container-microshift.pp

mkdir -p /etc/microshift/manifests

curl -s -L https://gitlab.com/nvidia/kubernetes/device-plugin/-/raw/main/deployments/static/nvidia-device-plugin-privileged-with-service-account.yml | sudo tee /etc/microshift/manifests/nvidia-device-plugin.yml

cat <<EOF | sudo tee /etc/microshift/manifests/kustomization.yaml
---
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - nvidia-device-plugin.yml
EOF

systemctl restart microshift

oc get pod -n nvidia-device-plugin
oc logs -n nvidia-device-plugin nvidia-device-plugin-jx8s8
oc get node -o json | jq -r '.items[0].status.capacity'

echo -e "[registries.search]\nregistries = ['nvcr.io']" | sudo tee /etc/containers/registries.conf
podman pull nvcr.io/nvidia/driver:535.104.12-rhel8.8

podman run --gpus all -d --name driver-container-nvidia \
  -u $(id -u):$(id -g) -e HOME=$HOME -e USER=$USER -v $HOME:$HOME \
  driver:535.104.12-rhel8.8
