## rebuff

Rebuff offers 4 layers of defense:

    Heuristics: Filter out potentially malicious input before it reaches the LLM.
    LLM-based detection: Use a dedicated LLM to analyze incoming prompts and identify potential attacks.
    VectorDB: Store embeddings of previous attacks in a vector database to recognize and prevent similar attacks in the future.
    Canary tokens: Add canary tokens to prompts to detect leakages, allowing the framework to store embeddings about the incoming prompt in the vector database and prevent future attacks.

```sh

podman build -t rebuff .

podman run -d -p 3000:3000 \
  -e OPENAI_API_KEY=<your_openai_api_key> \
  -e MASTER_API_KEY=12345 \
  -e BILLING_RATE_INT_10K=<your_billing_rate_int_10k> \
  -e MASTER_CREDIT_AMOUNT=<your_master_credit_amount> \
  -e NEXT_PUBLIC_SUPABASE_ANON_KEY=<your_next_public_supabase_anon_key> \
  -e NEXT_PUBLIC_SUPABASE_URL=<your_next_public_supabase_url> \
  -e PINECONE_API_KEY=<your_pinecone_api_key> \
  -e PINECONE_ENVIRONMENT=<your_pinecone_environment> \
  -e PINECONE_INDEX_NAME=<your_pinecone_index_name> \
  -e SUPABASE_SERVICE_KEY=<your_supabase_service_key> \
  -e REBUFF_API=http://localhost:3000 \
  --name rebuff rebuff

```
