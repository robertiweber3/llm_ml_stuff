## garak

```python

# parameters:

"--model_type",
"-m",
type=str,
help="module and optionally also class of the generator, e.g. 'huggingface', or 'openai'",

"--model_name",
"-n",
type=str,
default=None,
help="name of the model, e.g. 'timdettmers/guanaco-33b-merged'",

"--seed", "-s", type=int, default=None, help="random seed")

"--generations",
"-g",
type=int,
default=10,
help="number of generations per prompt",

"--probes",
"-p",
type=str,
default="all",
help="list of probe names to use, or 'all' for all (default).",

"--detectors",
"-d",
type=str,
default="",
help="list of detectors to use, or 'all' for all. Default is to use the probe's suggestion.",

"--eval_threshold",
type=float,
default=0.5,
help="minimum threshold for a successful hit",

"--deprefix",
action="store_false",
help="remove the prompt from the front of system output",

"--plugin_info",
type=str,
help="show info about one plugin; format as type.plugin.class, e.g. probes.lmrc.Profanity",

"--list_probes", action="store_true", help="list available vulnerability probes"

"--list_detectors", action="store_true", help="list available detectors"

"--list_generators",
action="store_true",
help="list available generation model interfaces",

"--version", "-V", action="store_true", help="print version info & exit"

"--verbose",
"-v",
action="count",
default=0,
help="add one or more times to increase verbosity of output during runtime",

"--generator_option",
"-G",
type=str,
help="options to pass to the generator",

"--probe_options",
"-P",
type=str,
help="options to pass to probes, formatted as a JSON dict",

"--report_prefix",
type=str,
default="",
help="Specify an optional prefix for the report and hit logs",

"--narrow_output",
action="store_true",
help="give narrow CLI output",

"--report",
"-r",
type=str,
help="process garak report into a list of AVID reports",

"--extended_detectors",
action="store_true",
help="If detectors aren't specified on the command line, should we run all detectors? (default is just the primary detector, if given, else everything)",

```
