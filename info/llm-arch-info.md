##

[Introduction to LLMs and the generative AI : Part 1 — LLM Architecture, Prompt Engineering and LLM Configuration](https://medium.com/@yash9439/introduction-to-llms-and-the-generative-ai-part-1-a946350936fd)

[Introduction to LLMs and the generative AI : Part 2 — LLM pre-training and scaling laws](https://medium.com/@yash9439/introduction-to-llms-and-the-generative-ai-part-2-llm-pre-training-and-scaling-laws-71e06c83998b)

[Introduction to LLMs and the generative AI : Part 3— Fine Tuning LLM with Instruction and Evaluation Benchmarks](https://medium.com/@yash9439/introduction-to-llms-and-the-generative-ai-part-3-fine-tuning-llm-with-instruction-and-326bc95e07ae)

[Generative AI with Large Language Models](https://www.coursera.org/learn/generative-ai-with-llms?action=enroll)

[deeplearning](https://www.deeplearning.ai)
