## Free courses for LLM/ML:

[CS324 - Large Language Models](https://stanford-cs324.github.io/winter2022/)

[COS 597G (Fall 2022): Understanding Large Language Models](https://www.cs.princeton.edu/courses/archive/fall22/cos597G/)

[Large Language Models, Spring 2023 - slides available](https://rycolab.io/classes/llm-s23/)

[CS224N: Natural Language Processing with Deep Learning](https://web.stanford.edu/class/cs224n/)

[Natural language processing (NLP) using libraries from the Hugging Face ecosystem](https://huggingface.co/learn/nlp-course/chapter1/1)
