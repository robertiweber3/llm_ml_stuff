## truss

<p align="center">
  <img src="https://miro.medium.com/v2/resize:fit:4800/format:webp/1*kf95pLFpCMu2qNnHR17VSA.jpeg" width="400" />
</p>

Think of it as Jenkins for LLM/ML models, serve that machine learning salad on a silver platter.
